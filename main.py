#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  main.py
#  
#  Copyright 2012 Al Pagan <generamics@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
import os, urllib2, simplejson, logging, anydbm, sys, tempfile, subprocess, gflags

def SortKeyed(items, itemArr):
  newarr = []
  for item in items:
    newarr.append(itemArr.index(item))
  return newarr

def GatherProgrammes():
  programmes = {}
  categories = []
  channels = []
  series = []
  cmd = ['get-iplayer', '--long', '--type', 'tv', '--listformat', '<index>|<name>|<thumbnail>|<episode>|<desc>|<channel>|<categories>']
  output = subprocess.check_output(cmd)
  for line in output.split('\n'):
    parts = line.split('|')
    if len(parts) == 7:
      (pid, name, thumb, ep, desc, channel, cats) = parts
      cats = cats.split(',')
      catList = []
      for cat in cats:
        if cat not in categories:
          categories.append(cat)
      if channel not in channels:
        channels.append(channel)
      if name not in series:
        series.append(name)
      programmes[pid] = {
        'pid': pid,
        'name': name,
        'thumb': thumb,
        'episode': ep,
        'description': desc,
        'channel': channel,
        'categories': cats
      }
  categories.sort()
  channels.sort()
  series.sort()
  for pid in programmes:
    programmes[pid]['categories'] = SortKeyed(programmes[pid]['categories'], categories)
  return {
    'programmes': programmes,
    'categories': categories,
    'channels': channels,
    'series': series
  }

def main():
  programmes = GatherProgrammes()
  print 'Content-Type: text/javascript; charset=utf-8\n'
  print simplejson.dumps(programmes)
  print

if __name__ == "__main__":
  main()
